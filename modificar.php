<?php
include "conexion.php";
$id = htmlentities($_GET['id']);
$sel = $con->query("SELECT * FROM inventario WHERE id = '$id';");
if($f = $sel->fetch_assoc()){

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Boostrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato|McLaren|Roboto&display=swap" rel="stylesheet">
    <title>CRUD</title>
</head>
<body>

    <nav class="navbar navbar-light bg-success">
        <a href="index.php" class="navbar-brand" style="color:white;font-family:Roboto;">CRUD</a>

    </nav>
    <div class="container" style="padding-top:30px;">
    <h1 style="align-text:center; padding-top:30px; padding-bottom:25px;">Modificar Producto</h1>
        <form action="actualizar.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $f['id'] ?>">
            <div class="form-group">
                <input type="text" name="producto" placeholder="Producto" class="form-control" value="<?php echo $f['producto']?>">
            </div>
            <div class="form-group">
                <input type="text" name="precio" placeholder="Precio" class="form-control" value="<?php echo $f['precio'] ?>">
            </div>
            <div class="form-group">
                <input type="number" name="cantidad" placeholder="Cantidad" class="form-control" value="<?php echo $f['cantidad'] ?>">
            </div>
            <div class="form-group">
                <input type="text" name="categoria" placeholder="Categoria" class="form-control" value="<?php echo $f['categoria'] ?>">
            </div>
            <div class="form-group">
                <input type="submit" value="Editar" class="btn btn-success">
            </div>        
        </form>

    </div>
    
    <!--Scripts-->
    <script src="https://kit.fontawesome.com/a77a9ce78b.js" crossorigin="anonymous"></script>
</body>
</html>